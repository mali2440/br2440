#!/bin/sh

WD=`pwd`
CYAN="\\033[1;36m"
NORMAL="\\033[0;39m"

echo "$CYAN" "> Cleaning image/ directory"
echo "$NORMAL"
rm -v $WD/image/u-boot.bin
# rm -v $WD/image/nand.bin
rm -v $WD/image/uImage


echo "$CYAN" "> Building all using Mali's config"
echo "$CYAN" "> Please wait or go drink coffe while rebuilding all"
echo "$CYAN" "> make mali_mini2440_defconfig && make"
echo "$NORMAL"
make mali_mini2440_defconfig && make


echo "$CYAN" "> Copying u-boot and kernel image in image/"
echo "$NORMAL"
cp -v $WD/output/images/u* $WD/image/
cd $WD/image/
# echo "$CYAN" "> Generating nand.bin"
# echo "$NORMAL"
# ./mknandimg.sh
echo "$CYAN" "> Here are the uboot and uImage."
echo "$CYAN" "> list image/ directory"
echo "$NORMAL"
ls -l

exit 0
