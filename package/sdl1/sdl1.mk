#
# sdl1
# basic building rules
#
SDL1_VERSION = 1.1
SDL1_SOURCE = sdl1-$(SDL1_VERSION).tgz
SDL1_SITE = http://helloworld.com/dl/$(HELLOWORLD_SOURCE)

SDL1_INSTALL_STAGING = YES
SDL1_INSTALL_TARGET = YES

define SDL1_BUILD_CMDS
    $(MAKE) CC="$(TARGET_CC)" LD="$(TARGET_LD)" -C $(@D) all
endef

define SDL1_INSTALL_STAGING_CMDS
    $(MAKE) DESTDIR=$(STAGING_DIR) -C $(@D) install
endef

define SDL1_INSTALL_TARGET_CMDS
    $(INSTALL) -D -m 0755 $(@D)/sdl1 \
                          $(TARGET_DIR)/bin/sdl1
endef

$(eval $(call GENTARGETS,package,sdl1))
