#
# sdlipttf
# basic building rules
#
SDLIPTTF_VERSION = 1.1
SDLIPTTF_SOURCE = sdlipttf-$(SDLIPTTF_VERSION).tar.gz
SDLIPTTF_SITE = http://helloworld.com/dl/$(SDLIPTTF_SOURCE)
SDLIPTTF_DEPENDENCIES = sdl sdl_ttf

define SDLIPTTF_BUILD_CMDS
    $(MAKE) CC="$(TARGET_CC)" LD="$(TARGET_LD)" -C $(@D) all
endef

define SDLIPTTF_INSTALL_TARGET_CMDS
    $(INSTALL) -D -m 0755 $(@D)/sdlipttf \
                          $(TARGET_DIR)/bin/sdlipttf
endef

$(eval $(call GENTARGETS,package,sdlipttf))
